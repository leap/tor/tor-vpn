package com.example.torwitharti.utils

enum class DummyConnectionState {
    IDLE, CONNECTING, CONNECTED, DISCONNECTED, CONNECTION_FAILED
}